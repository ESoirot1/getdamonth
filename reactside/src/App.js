import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="views">
          <Switch>
            <Route exact path="/" component={FindDaMonth} />
            <Route exact path="/result" component={Result} />
          </Switch>
        </div>
      </Router>
    );
  }
}

/*-----------------------------------------------------------------------------------------------------------------------------*/

class FindDaMonth extends Component {
  constructor(props) {
    super(props);
    this.state = { whatDaMonthInput: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.history.push('/result', {whatDaMonthInput: this.state.whatDaMonthInput });
  }
  
  handleChange(event) {
    this.setState({whatDaMonthInput: event.target.value})
  }

  render() {
    return (
      <div>        
        <form onSubmit={this.handleSubmit}>
          <label>
            Insert characters corresponding to the month you would like to find !<br/>
            <input type="text" name="whatDaMonthInput" value={this.state.whatDaMonthInput} onChange={this.handleChange} />
          </label>

          <input type="submit" value="submit"></input>
        </form>
      </div>
    )
  }
}

/*-----------------------------------------------------------------------------------------------------------------------------*/

class Result extends Component {
  constructor(props) {
    super(props);

    this.state = {
      months: [{value: "There are no months corresponding to your research ..."}],
      loading: true,
    }
  }

  componentDidMount() {
    const railsApiUrl = 'http://localhost:3100/months';
    axios.get(`${railsApiUrl}/${this.props.location.state.whatDaMonthInput}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    }).then((response) => {
        this.setState(
          { 
            months: response.data,
            loading: false,
          }
        )
        // this.state.months = response.body.months;
      }
    );
  }

  render() {
    return this.state.loading ? (
      <div>Loading...</div>
    ) : (
      <div className="container">
        Alright Lad, you found these months !
        <ul>
          {this.state.months.map((month) => {
            return <li key={month.id}>{month.name}</li>
          })}
        </ul>
      </div>
    )
  }
}

export default App;
